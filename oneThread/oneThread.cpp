#include <iostream>
#include <cstdlib>

// Calculates sin(x**3+x+5)+6cos(x**2+4x+6) in point x
double myFunc1D(double x);
// Finds maximum of 1D function func on the interval [start;stop] with the step dx 
double funcMax(double(*func)(double), double start, double stop, double dx);

int main(int argc, char const *argv[]){
  double start = 0, stop = 100, dx = 1e-4;

  if (argc != 4) {
    std::cout<<"Illigal number of arguments!\nUsage: start stop dx\n"<<
               "Default values are used."<<std::endl; 
  }
  else {
    start = atof(argv[1]);
    stop  = atof(argv[2]);
    dx    = atof(argv[3]);
  }
  
  std::cout<<"Find maximum between "<<start<<" and "<<stop<<" with "<<dx<<" step"<<std::endl;
  double maxValue = funcMax(start, stop, dx);
  std::cout<<"Maximum value: "<<maxValue<<std::endl;

  return 0;
}

double myFunc1D(double x){
  return sin(pow(x,3)+x+5)+6*cos(pow(x,2)+4*x+6);
}

double funcMax(double(*func)(double), double start, double stop, double dx){
  int nSteps = floor( (stop-start)/dx )-1; // exclude boundary values, check them separately
  double currentPoint = start;
  double currentValue = (*func)(currentPoint);

  double maxValue = currentValue; // check for start value

  for (int i = 0; i < nSteps; ++i){
    currentPoint += dx;
    currentValue = (*func)(currentPoint);
    if (maxValue < currentValue) maxValue = currentValue;
  }

  currentValue = (*func)(stop); // check for stop value
  if (maxValue < currentValue) maxValue = currentValue;

  return maxValue;
}