#ifndef __DOTASK_H__
#define __DOTASK_H__
#endif

#include <math.h>
// Calculates sin(x**3+x+5)+6cos(x**2+4x+6) in point x
float myFunc1D(float x);
// Finds maximum of myFunc1D function func on the interval [start;stop] with the step dx 
float funcMax(float start, float stop, float dx);
// Calculates maximum of array elements
float max(float* array, int length);

