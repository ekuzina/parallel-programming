# Usage pattern  
```bash
make syncSendRecv
mpirun -np 5 ./syncSendRecv 0 1.2 1e-4
```
`matrixTranspose` goal is to transpose matrix without usage MPI_Alltoall. Idea is to send swapping elements to processes with odd and even tags, swapping tags and sending them back.

# Docs, tutorials  
- [MPICH docs](https://www.mpich.org/static/docs/latest/www3/)  
- [Argonne National Laboratory lecture](https://anl.app.box.com/v/2019-ANL-MPI)
- [mpitutorial.com](https://mpitutorial.com/tutorials/)
- [some lecture](https://www3.nd.edu/~zxu2/acms60212-40212-S12/Lec-04.pdf)
- [pro-prof.com](https://pro-prof.com/archives/4386#asynch)