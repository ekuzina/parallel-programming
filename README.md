# Parallel Programming

MEPhI Parallel programming course.
The goal is to apply as many parallel methods and utilities as you can to do simple task.

## Progress

1. Simple one-thread function maximum calculating.
2. Four threads for calculating maximum in four sub-sections. Win API usage: CreateThread, HeapAlloc, CRITICAL_SECTION.
3. Win API usage: TLS(Thread Local Storage), LocalAlloc, CreateMutex.
4. MPI (MPICH): MPI_Send, MPI_Recv
5. MPI (MPICH): MPI_Isend, MPI_Irecv
5. MPI (MPICH): MPI_Comm_split
6. MPI (MPICH): MPI_Scatter, MPI_Reduce
7. OpenMP: reduction(max:var)
8. MPI (MPICH): MPI_Graph_create