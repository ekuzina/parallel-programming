# Usage pattern  
```bash
make ompReduce
./ompReduce 0 100 1e-4
```
# Docs, tutorials  
- [NGU tutorial](http://ccfit.nsu.ru/arom/data/openmp.pdf)  
- [pro-prof.com](https://pro-prof.com/archives/4335)