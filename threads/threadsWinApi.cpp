#include <iostream>
#include <cstdlib>
#include <cmath>
#include <windows.h>
#include <synchapi.h>

// Calculates sin(x**3+x+5)+6cos(x**2+4x+6) in point x
double myFunc1D(double x);
// Finds maximum of 1D function func on the interval [start;stop] with the step dx
// using Windows CreateThread and CRITICAL_SECTION
DWORD WINAPI funcMaxCreateThread( LPVOID lpParam );

#define NTHREADS 4
CRITICAL_SECTION cs;

typedef struct args {
    double(*func)(double);
    double start;
    double stop;
    double dx;
    double* maxValue;
} ARGS, *PARGS;


int main(int argc, char const *argv[]){
    double start = 0, stop = 100, dx = 1e-4;

    if (argc != 4) {
        std::cout<<"Illigal number of arguments!\nUsage: start stop dx\n"<<
            "Default values are used."<<std::endl;
    }
    else {
        start = atof(argv[1]);
        stop  = atof(argv[2]);
        dx    = atof(argv[3]);
    }
    std::cout<<"Find maximum between "<<start<<" and "<<stop<<" with "<<dx<<" step"<<std::endl;

    double* maxValue = new double(start);// shared variable
    InitializeCriticalSection(&cs);

    PARGS pThreadArgs[NTHREADS];
    DWORD   threadIds[NTHREADS];
    HANDLE  threadHandles[NTHREADS];
    HANDLE  threadHeapHandles[NTHREADS];

    double lenForOneThread = (stop-start)/NTHREADS;

    for( int i=0; i<NTHREADS; ++i ){
        threadHeapHandles[i] = HeapCreate(0,0,0);
        if( threadHeapHandles[i] == NULL ){
            std::cerr<<"HeapCreate error: "<<GetLastError()<<std::endl;
            delete maxValue;
            DeleteCriticalSection(&cs);
            ExitProcess(1);
        }
        pThreadArgs[i] = (PARGS)HeapAlloc(threadHeapHandles[i],HEAP_ZERO_MEMORY, sizeof(ARGS));
        if( pThreadArgs[i] == NULL ){
            std::cerr<<"HeapAlloc error: "<<GetLastError()<<std::endl;
            delete maxValue;
            DeleteCriticalSection(&cs);
            ExitProcess(2);
        }

        pThreadArgs[i]->func = &myFunc1D;
        pThreadArgs[i]->start = start+i*lenForOneThread;
        pThreadArgs[i]->stop = start+(i+1)*lenForOneThread;
        pThreadArgs[i]->dx = dx;
        pThreadArgs[i]->maxValue = maxValue;

        threadHandles[i] = CreateThread(NULL, 0, funcMaxCreateThread,
                                       pThreadArgs[i], 0, &threadIds[i]);


        if (threadHandles[i] == NULL) {
            std::cerr<<"CreateThread error: "<<GetLastError()<<std::endl;
//            delete(maxValue);
//            DeleteCriticalSection(&cs);
//            ExitProcess(3);
        }
    }

    WaitForMultipleObjects(NTHREADS, threadHandles, TRUE, INFINITE);
    std::cout<<"Maximum value: "<<*maxValue<<std::endl;

    for( int i=0; i<NTHREADS; ++i ){
        CloseHandle(threadHandles[i]);
        if(pThreadArgs[i] != NULL)
        {
            int returnValue = HeapFree(threadHeapHandles[i], 0, pThreadArgs[i]);
            if (returnValue == 0) {
                std::cerr<<"HeapFree error: "<<GetLastError()<<std::endl;
            }
            pThreadArgs[i] = NULL;
        }
    }

    delete(maxValue);
    DeleteCriticalSection(&cs);
    return 0;
}

double myFunc1D(double x){
    return sin(pow(x,3)+x+5)+6*cos(pow(x,2)+4*x+6);
}

DWORD WINAPI funcMaxCreateThread( LPVOID lpParam ){

    PARGS pArgs = (PARGS)lpParam;

    int nSteps = floor( (pArgs->stop-pArgs->start)/pArgs->dx )-1; // exclude boundary values, check them separately
    double currentPoint = pArgs->start;
    double currentValue = (*(pArgs->func))(currentPoint);

    double maxValueLocal = currentValue; // check for start value

    for (int i = 0; i < nSteps; ++i){
    currentPoint += pArgs->dx;
    currentValue = (*(pArgs->func))(currentPoint);
    if (maxValueLocal < currentValue) maxValueLocal = currentValue;
    }

    currentValue = (*(pArgs->func))(pArgs->stop); // check for stop value
    if (maxValueLocal < currentValue) maxValueLocal = currentValue;

    EnterCriticalSection(&cs);
    if (*(pArgs->maxValue) < maxValueLocal) *(pArgs->maxValue) = maxValueLocal;
    LeaveCriticalSection(&cs);

    return 0;
}
