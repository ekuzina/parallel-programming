#include <iostream>
#include <cstdlib>
#include <cmath>
#include <windows.h>
#include <synchapi.h>
#include <processthreadsapi.h>

double myFunc1D(double x);
DWORD WINAPI thProcess(LPVOID lpParam);
void WINAPI funcMax();

#define NTHREADS 4
DWORD tlsIndex;
HANDLE hMutex;

typedef struct args {
    double(*func)(double);
    double start;
    double stop;
    double dx;
    double* maxValue;
} ARGS, *PARGS;

typedef struct thVars {
    int nSteps;
    double currentPoint;
    double currentValue;
    double maxValueLocal;
    PARGS pArgs;
} THVARS, *PTHVARS;

int main(int argc, char const *argv[]){
    double start = 0, stop = 100, dx = 1e-4;

    if (argc != 4) {
        std::cout<<"Illigal number of arguments!\nUsage: start stop dx\n"<<
            "Default values are used."<<std::endl;
    }
    else {
        start = atof(argv[1]);
        stop  = atof(argv[2]);
        dx    = atof(argv[3]);
    }
    std::cout<<"Find maximum between "<<start<<" and "<<stop<<" with "<<dx<<" step"<<std::endl;

    double* maxValue = new double(start);// shared variable

    if ((tlsIndex = TlsAlloc()) == TLS_OUT_OF_INDEXES){
        std::cerr<<"TlsAlloc error: "<<GetLastError()<<std::endl;
        ExitProcess(1);
    }

    ARGS threadArgs[NTHREADS];
    DWORD   threadIds[NTHREADS];
    HANDLE  threadHandles[NTHREADS];

    hMutex = CreateMutex(NULL, FALSE, NULL);
    if (hMutex == NULL) {
        std::cerr<<"CreateMutex error: "<<GetLastError()<<std::endl;
        ExitProcess(2);
    }

    double lenForOneThread = (stop-start)/NTHREADS;
    for( int i=0; i<NTHREADS; ++i ){

        threadArgs[i].func = &myFunc1D;
        threadArgs[i].start = start+i*lenForOneThread;
        threadArgs[i].stop = start+(i+1)*lenForOneThread;
        threadArgs[i].dx = dx;
        threadArgs[i].maxValue = maxValue;

        threadHandles[i] = CreateThread(NULL, 0, thProcess,
                                       &threadArgs[i], 0, &threadIds[i]);


        if (threadHandles[i] == NULL) {
            std::cerr<<"CreateThread error: "<<GetLastError()<<std::endl;
        }
    }

    WaitForMultipleObjects(NTHREADS, threadHandles, TRUE, INFINITE);
    std::cout<<"Maximum value: "<<*maxValue<<std::endl;

    for( int i=0; i<NTHREADS; ++i ){
        CloseHandle(threadHandles[i]);
    }
    delete(maxValue);
    TlsFree(tlsIndex);

    return 0;
}

double myFunc1D(double x){
    return sin(pow(x,3)+x+5)+6*cos(pow(x,2)+4*x+6);
}

DWORD WINAPI thProcess( LPVOID lpParam ){

    PARGS pArgs = (PARGS)lpParam;

    PTHVARS threadVars;
    threadVars = (PTHVARS)LocalAlloc(LPTR, sizeof(THVARS));
    if (! TlsSetValue(tlsIndex, threadVars)) {
        std::cerr<<"TlsSetValue error: "<<GetLastError()<<std::endl;
        ExitThread(3);
    }
    threadVars->nSteps = floor( (pArgs->stop-pArgs->start)/pArgs->dx )-1; // exclude boundary values, check them separately
    threadVars->currentPoint = pArgs->start;
    threadVars->currentValue = (*(pArgs->func))(threadVars->currentPoint);
    threadVars->pArgs = pArgs;

    // Calculation
    funcMax();

    // Set result value
    DWORD waitResult = WaitForSingleObject(hMutex, INFINITE);
    if (waitResult == WAIT_OBJECT_0) {
        if (*(pArgs->maxValue) < threadVars->maxValueLocal)
        *(pArgs->maxValue) = threadVars->maxValueLocal;

        if (! ReleaseMutex(hMutex)){
            std::cerr<<"ReleaseMutex error: "<<GetLastError()<<std::endl;
            ExitProcess(4);
        }
    }

    threadVars = (PTHVARS)TlsGetValue(tlsIndex);
    if (threadVars != 0)
      LocalFree((HLOCAL) threadVars);

    return 0;
}

void WINAPI funcMax() {
    PTHVARS threadVars;
    threadVars = (PTHVARS)TlsGetValue(tlsIndex);

    threadVars->maxValueLocal = threadVars->currentValue; // check for start value
    for (int i = 0; i < threadVars->nSteps; ++i){
        threadVars->currentPoint += threadVars->pArgs->dx;
        threadVars->currentValue = (*(threadVars->pArgs->func))(threadVars->currentPoint);
        if (threadVars->maxValueLocal < threadVars->currentValue)
            threadVars->maxValueLocal = threadVars->currentValue;
    }
    threadVars->currentValue = (*(threadVars->pArgs->func))(threadVars->pArgs->stop); // check for stop value
    if (threadVars->maxValueLocal < threadVars->currentValue)
        threadVars->maxValueLocal = threadVars->currentValue;
}
